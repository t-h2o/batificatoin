#!/bin/sh

while true; do

	battery=$(cat /sys/class/power_supply/BAT0/capacity)
	notify-send 'Battery status' "The battery level is ${battery}%"
	sleep 15

done
